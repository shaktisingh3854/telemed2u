import { Module } from '@nestjs/common';
import { VideoConsultController } from './video-consult.controller';
import { VideoConsultService } from './video-consult.service';
import { TwilloModule } from '../twillo/twillo.module';

@Module({
  imports: [TwilloModule],
  controllers: [VideoConsultController],
  providers: [VideoConsultService],
})
export class VideoConsultModule {}
