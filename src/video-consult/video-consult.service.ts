import { Injectable } from '@nestjs/common';
import { AnyARecord } from 'dns';
import {TwilloService} from './../twillo/twillo.service';

@Injectable()
export class VideoConsultService {
    constructor(
        private twilloService : TwilloService
        ){}
    async generateGrantToken(userName: String, room: String) : Promise<String>{
        return  await this.twilloService.generateGrantToken(userName,room);
    }

    async createRoom(videoRoom:any):Promise<any>{
        return await this.twilloService.createRoom(videoRoom);
    }

    async getRoom(sid:any):Promise<any>{
        return await this.twilloService.getRoom(sid);
    }
    
}
