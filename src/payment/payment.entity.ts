import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('payment')
export class Payment {  
  @PrimaryGeneratedColumn()
  id: number; // Business Identifier for the payment noctice

  @Column({ length: 500 })
  status: string; //active | cancelled | draft | entered-in-error

  @Column('text')
  request: string; //

  @Column()
  response: string;

  @Column({ type: 'datetime' })
  created: Date;

  @Column()
  provider: string; //Practitioner | PractitionerRole | Organization

  @Column()
  payment:string;

  @Column({type:'date'})
  paymentDate:string;

  @Column()
  payee:string; //Practitioner | PractitionerRole | Organization
  //Definition - The party who will receive or has received payment that is the subject of this notification.

  @Column()
  recipient:string;  //Organization
  //Definition - The party who is notified of the payment status.

  @Column()
  amount:string;

  @Column()
  paymentStatus:string;
  //Definition - Typically paid: payment sent, cleared: payment received.

}