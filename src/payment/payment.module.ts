import { Module,forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { PaymentProvider } from './payment.providers';
import { PaymentService } from './payment.service';

@Module({
    imports:[DatabaseModule],
    providers:[...PaymentProvider,PaymentService]
})
export class PaymentModule {}
