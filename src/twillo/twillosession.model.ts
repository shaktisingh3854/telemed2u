import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TwilloSessionDocument = TwilloSession & Document;

@Schema()
export class TwilloSession {
  @Prop({ unique: true })
  attemptSid?: string;

  @Prop()
  phoneNumber?: string;

  @Prop({ default: new Date(), index: { expires: '60s' } })
  createdAt?: Date;
}

export const TwilloSessionSchema = SchemaFactory.createForClass(TwilloSession);
