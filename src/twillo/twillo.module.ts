import { Module, forwardRef } from '@nestjs/common';
import { TwilloService } from './twillo.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TwilloSessionSchema } from './twillosession.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'twillosession', schema: TwilloSessionSchema },
    ]),
  ],
  providers: [TwilloService],
  exports: [TwilloService],
})
export class TwilloModule {}
