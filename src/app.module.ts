import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { TwilloModule } from './twillo/twillo.module';
import { AuthModule } from './auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { VideoConsultModule } from './video-consult/video-consult.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { PaymentModule } from './payment/payment.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TwilloModule,
    UserModule,
    AuthModule,
    MongooseModule.forRoot('mongodb://localhost/sonoradb', {
      autoCreate: true,
    }),
    PassportModule.register({ session: true }),
    VideoConsultModule,
    DatabaseModule,
    PaymentModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
