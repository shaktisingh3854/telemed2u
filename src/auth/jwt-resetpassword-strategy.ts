import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { UserService } from '../user/user.service';
@Injectable()
export class JwtResetPasswordStrategy extends PassportStrategy(
  Strategy,
  'jwtresetpasswordStrategy',
) {
  private userService: UserService;
  constructor(private moduleref: ModuleRef) {
    super({
      jwtFromRequest: ExtractJwt.fromUrlQueryParameter('id'),
      ignoreExpiration: false,
      secretOrKey: 'JWT_RESET_PASSWORD_SECRET',
    });
  }
  onModuleInit() {
    this.userService = this.moduleref.get(UserService, { strict: false });
  }

  async validate(payload: any) {
    const user = await this.userService.findById(payload.req.id);
    if (user) return user;
  }
}
