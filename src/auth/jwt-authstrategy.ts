import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { ModuleRef } from '@nestjs/core';

@Injectable()
export class JwtAuthStrategy
  extends PassportStrategy(Strategy, 'jwtauthStrategy')
  implements OnModuleInit
{
  private userService: UserService;
  constructor(private moduleref: ModuleRef) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'JWT_SECRET',
    });
  }

  onModuleInit() {
    this.userService = this.moduleref.get(UserService, { strict: false });
  }

  async validate(payload: any) {
    const user = await this.userService.findById(payload.req.id);
    if (user) return user;
  }
}
