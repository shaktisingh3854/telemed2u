import { Module, forwardRef } from '@nestjs/common';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.model';
import { SigninModule } from './signin/signin.module';
import { SignupModule } from './signup/signup.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'users', schema: UserSchema }]),
    SigninModule,
    SignupModule,
    ForgotPasswordModule,
    AuthModule,
  ],

  controllers: [],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
{
}
