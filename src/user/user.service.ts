import { Injectable } from '@nestjs/common';
import { User, UserDocument } from './user.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthService } from '../auth/auth.service';
import * as _ from 'lodash';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('users') private readonly userModel: Model<UserDocument>,
    private authService: AuthService,
  ) {}
  async findOne(req: any): Promise<User> {
    return await this.userModel.findOne(req).lean().exec();
  }

  async findById(id: String) {
    return await this.userModel.findById(id).lean().exec();
  }

  async createUser(user): Promise<User> {
    const hashedPassword = await this.authService.hashPassword(user.password);
    const updatatedUser = { password: hashedPassword };
    user = _.extend(user, updatatedUser);
    return new this.userModel(user).save();
  }

  async updateOne(where: any, update: any) {
    return this.userModel.updateOne(where, update);
  }
  
  isUser(obj: any): obj is User {
    return "primaryMobileNo" in obj
 }

}
