import {
  Injectable,
  BadRequestException,
  HttpStatus,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthService } from '../../auth/auth.service';
import { User } from '../user.model';
import { TwilloService } from '../../twillo/twillo.service';
import { UserService } from '../user.service';
import { ModuleRef } from '@nestjs/core';
import * as _ from 'lodash';

@Injectable()
export class SigninService {
  private userService: UserService;
  constructor(
    private moduleref: ModuleRef,
    private twilloService: TwilloService,
    private authService: AuthService,
  ) {}

  onModuleInit() {
    this.userService = this.moduleref.get(UserService, { strict: false });
  }

  async getOtp(phoneNumber: string, channel: string): Promise<any> {
    const { status, attemptSid } = await this.twilloService.sendSMS(
      phoneNumber,
      channel,
    );
    if (status == 'pending') {
      await this.twilloService.insertIntoTwilloSession({
        attemptSid,
        phoneNumber,
        createdAt: new Date(),
      });
      var jwt = await this.authService.generateJWT(
        { attemptSid },
        { secret: 'JWT_OTP_VERIFY_SECRET', expiresIn: '180s' },
      );

      return { status, accessToken: jwt };
    } else throw new BadRequestException({ twilloError: true });
  }

  async completeProfile(
   completeProfileRequest,user
  ) {
    const {address,securityQestion1,securityQestion2,securityAnswer2}=completeProfileRequest
    return await this.userService.updateOne(
      { _id:user["_id"]},
      { address,securityQestion1,securityQestion2,securityAnswer2 },
    );
  }
  async loginWithOtp(otp, phoneNumber) {
    const status = await this.twilloService.verifyOtp(otp, phoneNumber);
    if (status == 'approved') {
      const user = await this.userService.findOne(
        {$or:[{primaryMobileNo: phoneNumber},
              {mobileNumber1:phoneNumber},
              {mobileNumber2:phoneNumber}]});
      if (!user)
        throw new UnauthorizedException({
          httpStatus: HttpStatus.FORBIDDEN,
          response: {userNotExist:true},
          twilloStatus: status,
        });
        
      return this.login(user);
    } else {
      throw new BadRequestException({ twilloStatus: status });
    }
  }

  private async login(user): Promise<any> {
    const payload = { id: user._id };
    const jwt = await this.authService.generateJWT(payload, {
      secret: 'JWT_SECRET',
      expiresIn: '300s',
    });
    const resUser = _.omit(user, ['password','resetLink']) as User

    return {
      ...resUser,
      accessToken: jwt,
      isProfileComplete: this.isProfileComplete(user),

    };
  }

  async loginUserWithEmail(email: string, password: string): Promise<any> {
    const res = await this.validateUserWithEmail(email, password);
    if (this.userService.isUser(res)) return await this.login(res);
    else throw new UnauthorizedException(res);
  }
 
  private isProfileComplete(user: User) {
    const { address, securityQestion1, securityQestion2 } = user;
    return address != '' && securityQestion1 != '' && securityQestion2 != '';
  }

  async validateUserWithEmail(
    email: string,
    password: string,
  ): Promise<User | any> {
    let isUserExist: boolean = false;
    const user = await this.userService.findOne({ email });
    if (user) {
      isUserExist = true;
      const isPasswordMatched = await this.authService.comparePasswords(
        password,
        user.password,
      );
      if (isPasswordMatched) {
        const resUser = _.omit(user, ['password']) as User
        return resUser;
      } else return { isPasswordMatched };
    }
    return { isUserExist };
  }
}
