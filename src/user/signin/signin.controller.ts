import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Get,
  UseFilters,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/guards/jwt-guard';
import { JwtOtpAuthGuard } from '../../auth/guards/jwt-otp-guard';
import { SigninService } from './signin.service';
import { HttpExceptionFilter } from '../../http-exception-filter';
import { CompleteProfileRequestDto } from '../dto/user.dto';

@UseFilters(new HttpExceptionFilter())
@Controller()
export class SigninController {
  constructor(private signinService: SigninService) {}

  @Post('login')
  async loginWithEmail(
    @Body('email') email: string,
    @Body('password') password: string,
  ) {
    return this.signinService.loginUserWithEmail(email, password);
  }
  @Post('getotp')
  async getotp(
    @Body('phoneNumber') phoneNumber: string,
    @Body('channel') channel: string,
  ) {
    const res = await this.signinService.getOtp(phoneNumber, channel);
    return res;
  }

  @UseGuards(JwtOtpAuthGuard)
  @Post('verifylogin')
  async verifylogin(@Body('otp') otp: number, @Request() req) {
    const { phoneNumber } = req.user;
    const res = await this.signinService.loginWithOtp(otp, phoneNumber);
    return { message: res };
  }
  @UseGuards(JwtAuthGuard)
  @Post('completeprofile')
  async completeProfile(
    @Body() completeProfileDto:CompleteProfileRequestDto,
    @Request() req,
  ) {
    const {user}=req
    return this.signinService.completeProfile(
      completeProfileDto,
      user
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get('home')
  async home(@Request() req) {
    return req.user;
  }
}
