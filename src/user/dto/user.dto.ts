import {
  isEmpty,
  IsString,
  IsNotEmpty,
  isNotEmpty,
  IsDate,
  IsEmail,
} from 'class-validator';
import { PickType } from '@nestjs/mapped-types';
import { Type } from 'class-transformer';
export class UserRequstDto {
  @IsString()
  @IsNotEmpty()
  readonly fname: String;

  @IsString()
  @IsNotEmpty()
  readonly lname: String;

  @IsDate()
  @Type(() => Date)
  @IsNotEmpty()
  readonly dob: Date;

  @IsString()
  @IsNotEmpty()
  readonly password: String;

  @IsEmail()
  @IsNotEmpty()
  readonly email: String;

  @IsString()
  readonly address: String;

  @IsString()
  readonly securityQestion1: String;

  @IsString()
  readonly securityAnswer1: String;

  @IsString()
  readonly securityQestion2: String;
  @IsString()
  readonly securityAnswer2: String;
}
export class CreateUserDto extends PickType(UserRequstDto, [
  'fname',
  'lname',
  'dob',
  'password',
  'email',
] as const) {}

export class CompleteProfileRequestDto extends PickType(UserRequstDto, [
  'address',
  'securityQestion1',
  'securityAnswer1',
  'securityQestion2',
  'securityAnswer2'
] as const) {}



