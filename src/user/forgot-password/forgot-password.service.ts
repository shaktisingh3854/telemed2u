import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthService } from '../../auth/auth.service';
import { ModuleRef } from '@nestjs/core';
import { UserService } from '../user.service';
import * as nodemailer from 'nodemailer';

@Injectable()
export class ForgotPasswordService {
  private userService: UserService;
  private transporter;
  constructor(private moduleref: ModuleRef, private authService: AuthService) {
    this.transporter = nodemailer.createTransport({
      service: process.env.EMAIL_SERVICE,
      auth: {
        user: process.env.SENDER_EMAIL,
        pass: process.env.SENDER_EMAIL_PASSWORD,
      },
    });
  }

  async onModuleInit() {
    this.userService = this.moduleref.get(UserService, { strict: false });
  }

  async forgetPassword(email: String) {
    const user = await this.userService.findOne({ email });
    if (user) {
      const userId = user['id'];
      const jwt = await this.authService.generateJWT(
        { id: userId },
        { secret: 'JWT_RESET_PASSWORD_SECRET', expiresIn: '1200s' },
      );
      await this.userService.updateOne({ _id: userId }, { resetLink: jwt });
      this.sendEmail(email, jwt);
    } else {
      throw new UnauthorizedException();
    }
  }

  async sendEmail(email: String, token: String): Promise<any> {
    try {
      const mailOptions = {
        from: process.env.SENDER_EMAIL,
        to: email,
        subject: 'Forget Password link',
        html: `<h3> Please click on given link <h3>
          <p> ${process.env.UI_SERVER_URL}/reset-password?id=${token}<p>`,
      };
      this.transporter.sendMail(mailOptions);
      return { emailSent: true };
    } catch (e) {
      throw new HttpException({ emailSent: false }, HttpStatus.BAD_REQUEST);
    }
  }

  validatePasswordResetLink(user): boolean {
    if (user.resetLink == '') return true;
    else return false;
  }
  async resetPassword(user, newPassword: string) {
    const id = user['id'];
    const hashedPassword = await this.authService.hashPassword(newPassword);
    await this.userService.updateOne(
      { _id: id },
      { password: hashedPassword, resetLink: '' },
    );
  }
}
