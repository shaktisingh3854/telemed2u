import {
  Controller,
  Post,
  Body,
  Get,
  Query,
  UseGuards,
  Res,
  Request,
  UseFilters,
} from '@nestjs/common';
import { ForgotPasswordService } from './forgot-password.service';
import { JwtResetPasswordGuard } from '../../auth/guards/jwt-resetpassword-guard';
import { Response } from 'express';
import { HttpExceptionFilter } from '../../http-exception-filter';

@UseFilters(new HttpExceptionFilter())
@Controller()
export class ForgotPasswordController {
  constructor(private forgetPasswordService: ForgotPasswordService) {}
  @Post('forget-password')
  async forgetPassword(@Body('email') email: String, @Res() res: Response) {
    this.forgetPasswordService.forgetPassword(email);
    res.send({ message: 'Email sent' });
  }

  @UseGuards(JwtResetPasswordGuard)
  @Get('reset-password')
  async resetPassword(@Query('id') query: String, @Request() req) {
    const user = req.user;
    const isPasswordAlreadyUpdated =
      this.forgetPasswordService.validatePasswordResetLink(user);
    return { isPasswordAlreadyUpdated };
  }

  @UseGuards(JwtResetPasswordGuard)
  @Post('reset-password')
  async resetPasswordPost(
    @Query('id') id: String,
    @Body('password') newPassword: string,
    @Request() req,
  ) {
    const user = req.user;
    this.forgetPasswordService.resetPassword(user, newPassword);
  }
}
