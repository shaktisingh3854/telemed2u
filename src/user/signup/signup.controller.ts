import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Response,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  UseFilters,
  BadRequestException,
} from '@nestjs/common';
import { JwtOtpAuthGuard } from '../../auth/guards/jwt-otp-guard';
import { SignupService } from './signup.service';
import { CreateUserDto } from '../dto/user.dto';
import { HttpExceptionFilter } from '../../http-exception-filter';

@UseFilters(new HttpExceptionFilter())
@Controller()
export class SignupController {
  constructor(private signupService: SignupService) {}

  @UseGuards(JwtOtpAuthGuard)
  @Post('register')
  @UsePipes(new ValidationPipe())
  async registerWithOtp(
    @Body() user: CreateUserDto,
    @Request() req,
    @Response() res,
  ) {
 
    const { phoneNumber } = req.user;
    const loginToken = await this.signupService.createUserWithOtp(
      user,
      phoneNumber,
    );
    if (loginToken)
      return res
        .status(HttpStatus.OK)
        .json({ messge: 'User created', accessToken: loginToken });
    else throw new BadRequestException();
  }
}
