import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../user.service';
import { ModuleRef } from '@nestjs/core';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class SignupService {
  private userService: UserService;
  constructor(private moduleref: ModuleRef, private authService: AuthService) {}

  onModuleInit() {
    this.userService = this.moduleref.get(UserService, { strict: false });
  }

  async createUserWithOtp(user, phoneNumber: string) {
    user = { ...user, primaryMobileNo: phoneNumber };
    const isUserExist = await this.validateUser(user);
    if (!isUserExist) {
      const newUser = await this.userService.createUser(user);
      const payload = { id: newUser['_id'] };
      return await this.authService.generateJWT(payload, {
        secret: 'JWT_SECRET',
        expiresIn: '300s',
      });
    } else throw new BadRequestException({ isUserExist });
  }

  async validateUser(user): Promise<Boolean> {
    const { fname, lname, dob, primaryMobileNo } = user;
    const result = await this.userService.findOne({
      fname,
      lname,
      dob,
      primaryMobileNo,
    });
    if (result) return true;
    else return false;
  }

  // async updateWithMobileNo(user, phoneNumber) {
  //   if (!user.mobilenumber2) {
  //     return this.userModel.updateOne({ user }, { mobilenumber2: phoneNumber });
  //   } else if (!user.mobilenumber)
  //     return this.userModel.updateOne({ user }, { mobilenumber2: phoneNumber });
  // }
}
