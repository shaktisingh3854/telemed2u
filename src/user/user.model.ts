import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({index:{unique:true} })
  primaryMobileNo?: string;

  @Prop({ default: '' })
  fname?: string;

  @Prop({ default: '' })
  lname?: string;

  @Prop({ default: Date.now() })
  dob?: Date;

  @Prop({ default: '' })
  password?: string;

  @Prop({ default: '' })
  email?: string;

  @Prop({ default: '' })
  mobileNumber2?: string;

  @Prop({ default: '' })
  mobileNumber3?: string;

  @Prop({ default: '' })
  address?: string;

  @Prop({ default: '' })
  securityQestion1?: string;

  @Prop({ default: '' })
  securityAnswer1?: string;

  @Prop({ default: '' })
  securityQestion2?: string;

  @Prop({ default: '' })
  securityAnswer2?: string;

  @Prop({ default: '' })
  resetLink?: string;

  @Prop()
  lastLoggedIn:Date;
}


export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.index({fname:1,lname:1,dob:1,primaryMobileNo:1},{unique:true})
